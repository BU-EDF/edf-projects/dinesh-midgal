# Dinesh Midgal

## Description
This repository contains BU EDF's work, lead by Prof. Dinesh Loomba at UNM and Prof. Ed Kearns at BU, for a small-scale experiment to detect and study the Migdal effect.  

This repository is comprised on submodules that form the different hardware, firmware, and software files required.

To download files locally, you can clone this repository:
```git clone git@gitlab.com:BU-EDF/dinesh-migdal/dinesh-migdal.git```

Next, to download submodules locally:
```git submodule update --init --recursive```

### Structure
- Firmware
    - [sbnd-femb-firmware](https://gitlab.com/BU-EDF/dinesh-migdal/sbnd-femb-firmware) (Quartus project)
    - [zcu102-firmware](https://gitlab.com/BU-EDF/dinesh-migdal/zcu102-firmware) (Vivado project)
- Hardware
    - [migdal-transition-board](https://gitlab.com/BU-EDF/dinesh-migdal/migdal-transition-board) (Allegro project for board connecting SBNDs to ZCU102)
    - [sbnd-analog-input-adapter](https://gitlab.com/BU-EDF/dinesh-migdal/sbnd-analog-input-adapter) (Allegro project for board connecting detector plate to SBNDs)
    - [migdal-system-cad](https://gitlab.com/BU-EDF/dinesh-migdal/migdal-system-cad) (Inventor assembly that includes spacing for SBND Analog Input Adapter)
- Software
    - [sbnd-femb-scripts](https://gitlab.com/BU-EDF/dinesh-migdal/sbnd-femb-scripts) (Python scripts for communicating with SBND boards over Ethernet)
    - [zcu102-software](https://gitlab.com/BU-EDF/dinesh-migdal/zcu102-software) (C scripts for DMA requests on ZCU102, Python scripts for lab computer to communicate with ZCU102)

## System Overview
The 256 x 256 channel system contains 4 SBND FEMBs, 1 ZCU102, and 2 transition boards. Each SBND FEMB amplifies and digitizes 128 analog signals from the GEM strip readout. The data is then sent in real-time to the ZCU102 for storage. The transition boards are required to provide an adapter from the Mini-SAS connectors on the SBND board to the VITA 57.1 connectors on the ZCU102.

The maximum sample frequency is 3 MHz with a maximum of 1 ms of pretrigger data.

![System Overview](docs/system_overview.png)

## Roadmap
- Fix DMA bugs
- Test SBND Analog Input Adapters

## Project status
In Progress (June 11th, 2024 to TBD)

## QR Code Generation in Allegro Cadence
1. Close Allegro Cadence
2. Run the `AllegroQRCodeGenerator172.exe` in `hardware/lib/QR` and ensure the template is there
3. Specify the path to your Allegro Cadence install if needed and fill out all values
4. Generate
5. Move drawing to Mechanical folder on path for parts
6. Insert as mechanical part
